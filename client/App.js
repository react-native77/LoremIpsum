import 'react-native-gesture-handler';
import Animated from 'react-native-reanimated';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import AppStack from './src/navigation/AppStack';
import AuthStack from './src/navigation/AuthStack';
import Routes from './src/navigation/Routes';


const App = () => {
	return (
		<Routes />
	);
}
export default App


import { SafeAreaView, Text, View, StyleSheet, Image } from 'react-native'
import React, { Component } from 'react'
import { Dimensions } from "react-native";

var width = Dimensions.get('window').width; 


export class LoginScreen extends Component {
  render() {
    return (
        <SafeAreaView style={styles.areaView}>
            <View style={styles.content}>                
                <Image source={require('../img/logo.png')} style={styles.logo} />               
                <Text style={styles.title}> Se connecter </Text>
                <View style={styles.googleAuht}>
                    <Text>Connect with google</Text>
                </View>                
            </View>
        </SafeAreaView>
    )
  }
}

export default LoginScreen

const styles = StyleSheet.create({
    areaView: {
        flex: 1,
        justifyContent: 'center',	
        alignItems: 'center',
        backgroundColor: "rgb(255, 83, 7)",
        
    },
    content: { },
    logo: {
        flex: 2,
		width: width,
		height: 400,
        
	},
    title: {
        flex: 2,        
    },
    googleAuht: {
        flex: 1,
    }
})
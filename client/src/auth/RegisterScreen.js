import { View, Text, TextInput, TouchableOpacity, Image, StyleSheet} from 'react-native'
import React, {useEffect, useState} from 'react'
import { SafeAreaView } from 'react-native-safe-area-context'
import logo from '../img/logo1.png'
import { ScrollView } from 'react-native-gesture-handler'


const RegisterScreen = () => {

	const [ username, setUsername] = useState('')
	const [email, setEmail] = useState('')
	const [ password, setPassword ] = useState('')
	const [passwordCheck, setPasswordCheck] = useState();
    const [error, setError] = useState();


	const registerUser = async(e) => {
		try {

			const userData = { username, email, password, passwordCheck }
			const registerUser = await Axios
				.post('http://localhost:8080/users/register', userData)
			
			console.log(userData)
				
			
		} catch (error) {
			error.response.data.message && setError(error.response.data.message);    
			console.log(`Echec de l'enregistrement : ${error.response.data.message}`)   
		}
	}
	


  return (
	<ScrollView style={styles.container}>
		<SafeAreaView>
			
			<View style={styles.logoContainer}> 
				<Image
					style={styles.logo}
					source={logo}            
					/>
					<Text style={styles.title}>Créer un compte</Text>
			</View>
			

			<View style={styles.formContainer}>				
					<View style={styles.inputContainer}>
						<TextInput
							style={styles.inputBox} 
							placeholder="Pseudo"
							placeholderTextColor="white"
							value={username}
							onChangeText={(username) => setUsername(username)}
							blurOnSubmit={false}
							autoFocus={true}					
						/>
					</View>
					<View style={styles.inputContainer}>
						<TextInput
							style={styles.inputBox} 
							placeholder='Email'
							placeholderTextColor="white"
							value={email}
							onChangeText={(email) => setEmail(email)}	
							blurOnSubmit={false}
							autoFocus={true}
						/>
					</View>
					<View style={styles.inputContainer}>
						<TextInput
							style={styles.inputBox} 
							placeholder='Mot de passe'
							placeholderTextColor="white"
							value={password}
							onChangeText={(password) => setPassword(password)}						
							blurOnSubmit={false}
							autoFocus={true}
						/>
					</View>
					<View style={styles.inputContainer}>
						<TextInput
							style={styles.inputBox} 
							placeholder='Confirmer mot de passe'
							placeholderTextColor="white"
							value={passwordCheck}
							onChangeText={(passwordCheck) => setPasswordCheck(passwordCheck)}	
							blurOnSubmit={false}
							autoFocus={true}					
						/>
					</View>
					<TouchableOpacity
						activeOpacity={0.5}
						onPress={registerUser}
						style={styles.buttonSubmit}
						>
						<Text style={styles.textSubmit}>Créer</Text>
				</TouchableOpacity>
			</View>
		</SafeAreaView>
	</ScrollView>
	)
}

export default RegisterScreen


const styles = StyleSheet.create({
	container: {
		backgroundColor: "rgb(255, 83, 7)"
	},
	title: {
		fontSize: 22,
		textTransform: 'uppercase',
		color: 'white',
	},
	logoContainer: {		
		height:200,		
		justifyContent:'center',
		alignItems:'center',
		marginTop: 70
	},
	logo: {
		width: 200,
		height: 200,
	},
	formContainer: {
		justifyContent:'center',
		alignItems:'center',
		textAlign: 'left',
		marginTop: 50,
	},
	registerTitle: {
		marginTop: 10,
		marginBottom: 20,
		width: 300,
		justifyContent: 'center'
	},
	inputContainer: {
		height: 40,
		marginBottom: 5,
		alignItems: 'center'
	},
	inputBox: {
		width: 200,
		borderWidth:1,
		borderColor: "transparent",
		borderBottomColor: "white",
		color: 'white'
	},
	buttonSubmit: {
		marginTop: 25,
		width: 130,
		borderWidth:1,
		borderColor:"black",
		backgroundColor: 'black',
		height: 50,
		borderRadius: 5,
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	textSubmit: {
		color: 'white', 
		fontSize: 18,
		textTransform: "uppercase",		
	}
})
import { Text, View, ScrollView, StyleSheet, Image, Pressable } from 'react-native'
import React, { Component } from 'react'
import { SafeAreaView } from 'react-native-safe-area-context'

import SearchBar from '../components/SearchBar'
import Carousel from '../components/home/Carousel'



export class HomeScreen extends Component {
  render() {
    return (        
        <SafeAreaView  style={styles.sAv}>
            <ScrollView style={styles.homeContainer} >
                <View style={styles.homeViewContainer}>

                    <SearchBar />

                    <View style={styles.newContainer}>
                        <Pressable style={styles.button} onPress={ () => navigation.navigate("New")}>
                            <Text style={styles.text}>Ajouter une recette</Text>
                        </Pressable>
                    </View>

                    <View style={styles.title2Container}> 
                        <Text style={styles.title2}>Ajoutées récemment</Text>
                        <Text style={styles.seeAll}>Voir +</Text>
                    </View>                   

                    <View style={styles.carouselContainer}>
                        <Carousel />
                    </View>   

                    <View style={styles.gridContainer}>
                        <Text style={styles.title2}>Catégories</Text>
                        <View style={styles.grid}>
                            <Image source={require('../img/recettes/e1.jpg')} style={styles.catImage} />
                            <Text style={styles.catText}>Entrées</Text>
                        </View>
                        <View style={styles.grid}>
                            <Image source={require('../img/recettes/e2.jpg')} style={styles.catImage} />
                            <Text style={styles.catText}>Plats</Text>
                        </View>
                        <View style={styles.grid}>
                            <Image source={require('../img/recettes/e3.jpg')} style={styles.catImage} />
                            <Text style={styles.catText}>Desserts</Text>
                        </View>
                        <View style={styles.grid}>
                            <Image source={require('../img/recettes/e4.jpg')} style={styles.catImage} />
                            <Text style={styles.catText}>Cocktails</Text>
                        </View>
                    </View>



                </View>
            </ScrollView>
           
      </SafeAreaView>
    )
  }
}

export default HomeScreen

const styles = StyleSheet.create({
    sAv: {
        backgroundColor: "white", 
    },
    homeContainer: {
        marginTop: 20,
    },
    homeViewContainer :{
        paddingHorizontal : 20,
        flex: 1
    },
    title2Container: {
        flexDirection: 'row', 
        justifyContent: 'space-between',
        alignItems: 'baseline',
        marginTop: 20
    },
    title2: {
        fontSize: 15,
        textTransform: 'uppercase', 
        color: "black",
        fontWeight: 'bold',
        marginVertical: 20,
    },
    seeAll: {
        color: "rgb(255, 83, 7)"
    },
    newContainer: {
        borderWidth: 1,
        borderColor: "black",
        height: 50,
        marginVertical: 20,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    carouselContainer: {
        marginBottom: 50, 
    },
    gridContainer: {
    },
    grid: {
        flex: 1,
        height: 150,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,

    },
    catImage: {
        flex: 1,
        resizeMode:'cover',
        width: "100%",
        position: 'absolute',
        height: 150,        
    }, 
    catText: {
        borderWidth: 1,
        paddingHorizontal: 30,
        paddingVertical: 15,
        backgroundColor: 'rgba(255, 83, 7, .4)',
        borderColor: 'rgb(255, 83, 7)',
        color: "white",
        fontWeight: 'bold', 
        fontSize: 20,
        borderRadius: 5,
    }
   
})
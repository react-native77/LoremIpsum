import React from 'react';
import { SafeAreaView, StyleSheet, Text, View, Image, Pressable } from 'react-native';
import { Dimensions } from "react-native";



var width = Dimensions.get('window').width; 


const MainScreen = ({ navigation }) => {
    return (
      <SafeAreaView style={styles.container}>
          <View style={styles.content}>
              <Image source={require('../img/logo.png')} style={styles.logo} />
  
              <Text style={styles.brand}> MERLO PIMUS </Text>
  
              <Pressable style={styles.button} onPress={ () => navigation.navigate("Home")}>
                  <Text style={styles.text}>Entrer</Text>
              </Pressable>
              {/* <Pressable style={styles.button} onPress={ () => navigation.navigate("Login")}>
                  <Text style={styles.text}>Entrer</Text>
              </Pressable> */}
          </View>
      </SafeAreaView>
    );
}

export default MainScreen


const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	content: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		width: width,
		backgroundColor: "rgb(255, 83, 7)"
	},
	logo: {
		width: width,
		height: 400		
	},
	brand: {
		width: width, 
		height: 60,
		textAlign: "center",
		fontSize: 30, 		
		paddingBottom: 10, 
		marginBottom: 30,
		fontWeight: "bold"
	},
	button: {
		alignItems: 'center',
		justifyContent: 'center',
		paddingVertical: 25,
		paddingHorizontal: 75,
		borderRadius: 4,
		elevation: 3,
		backgroundColor: 'black',
		width: 250, 
		height: 80,
		borderRadius: 10	
	  },
	  text: {
		fontSize: 25,
		fontWeight: 'bold',
		letterSpacing: 0.25,
		color: 'white',
	  },
});

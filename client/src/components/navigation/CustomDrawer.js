import { View, Text, StyleSheet, ImageBackground, Image } from 'react-native'
import React from 'react'

import { DrawerContentScrollView, DrawerItemList } from '@react-navigation/drawer'




const CustomDrawer = (props) => {
  return (
    <View style={{flex:1}}>
        <DrawerContentScrollView 
            {...props}
            contentContainerStyle={styles.customDrawerContainer} 
        >
            <View style={styles.facade}></View>
            <Image style={styles.imgBg} source={require('../../img/recettes/e2.jpg')} /> 
            <View style={styles.profilPicContainer}>
                <Image style={styles.logo} source={require('../../img/logo1.png')} />                  
                <Text style={styles.logoText}>LOREM IPSUM</Text>
            </View> 
            <DrawerItemList {...props} />
            
        </DrawerContentScrollView>

        
    </View>
  )
}

export default CustomDrawer

const styles = StyleSheet.create({
    customDrawerContainer: {
        position: "relative"
    },
    facade: {
        position: "absolute",
        zIndex: 1, 
        height:200,
        backgroundColor: "rgba(255, 83, 7, .8)",
        top: 28,
        width: "100%",
    },
    imgBg: {
        width: "100%",
        height: 200,

    },    
    profilPicContainer: {
        position: "absolute",
        top: 30,
        left: 0,
        zIndex: 2,
        width: "100%",
        textAlign:"center",
        justifyContent: "center"
    },
    logo: {
        width: 175,
        height: 175,
        textAlign:"center",
        marginLeft: 50
    },
    logoText: {
        textAlign: 'center', 
        color: "white",
    }    
})
import { Text, View, StyleSheet, TextInput } from 'react-native'
import React, { Component } from 'react'
import Icon from 'react-native-vector-icons/Ionicons';



export default function SearchBar() {

 
    return (
        <View style={styles.inputContainer}>
            <Icon name="search-outline" style={styles.searchicon}></Icon>
            <TextInput
                placeholder="Rechercher"
                onPress={()=>console.log('je cherche ... ')}
            />
        
        </View>
    )

}


const styles = StyleSheet.create({
   inputContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: 'center',
    borderWidth: 2,
    textAlign: "left", 
    paddingHorizontal: 10,
    height: 50,
    backgroundColor: "white",
    borderRadius: 5,
   }, 
   searchicon: {
    marginRight: 10,
    fontSize: 15
   }
});




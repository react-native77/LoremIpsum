import { Text, View, StyleSheet, Image } from 'react-native'
import React, { Component } from 'react'
import Swiper from 'react-native-swiper'



export class Carousel extends Component {
  render() {
    return (
        <Swiper style={styles.wrapper} showsButtons={true}>
            <View style={styles.slide1}>
                <Image source={require('../../img/recettes/idk.jpg')} style={styles.carouselImage} />
                <Text style={styles.text}>Delicious burger</Text>
            </View>
            <View style={styles.slide2}>
              <Image source={require('../../img/recettes/burger.jpg')} style={styles.carouselImage} />
              <Text style={styles.text}>Beautiful taboulé</Text>
            </View>
            <View style={styles.slide3}>
              <Image source={require('../../img/recettes/soup.jpg')} style={styles.carouselImage} />
              <Text style={styles.text}>Simple starter</Text>
            </View>
      </Swiper>
    )
  }
}

export default Carousel

const styles = StyleSheet.create({
    wrapper: {
        height: 200,        
    },
    slide1: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#9DD6EB',
      position: 'relative',
      zIndex: 1
    },
    carouselImage: {
        flex: 1,
        resizeMode:'cover',
        width: "100%",
        position: 'absolute',
        position: "absolute", 
        height: 200, 
               
    },  
    slide2: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#97CAE5'
    },
    slide3: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#92BBD9'
    },
    text: {
      color: '#fff',
      fontSize: 30,
      fontWeight: 'bold'
    }
  })
import { View, Text } from 'react-native'
import React from 'react'
import 'react-native-gesture-handler';
import Animated from 'react-native-reanimated';
import { NavigationContainer } from '@react-navigation/native';

import AppStack from './AppStack';
import AuthStack from './AuthStack';

const Routes = () => {
  return (

      <NavigationContainer >
			  <AppStack />
			  {/* <AuthStack />*/}
		</NavigationContainer>
    
  )
}

export default Routes
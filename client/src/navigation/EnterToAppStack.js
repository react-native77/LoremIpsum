import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack'

import MainScreen from '../screens/MainScreen';
import HomeScreen from '../screens/HomeScreen';

const Stack = createNativeStackNavigator();

const EnterToAppStack = () => {
    return (
        <Stack.Navigator initialRouteName='Main' options={{headerShown: false}}>
            <Stack.Screen name='Main' component={MainScreen} />
            <Stack.Screen name='Home' component={HomeScreen} />            
        </Stack.Navigator>

    )
}

export default EnterToAppStack

const styles = StyleSheet.create({})
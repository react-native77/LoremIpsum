import { Text, View } from 'react-native'
import React, { Component } from 'react'

import { createNativeStackNavigator } from '@react-navigation/native-stack';
const Stack = createNativeStackNavigator()

import MainScreen from '../screens/MainScreen';
import LoginScreen from '../auth/LoginScreen';



const AuthStack = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Main" component={MainScreen} options={{headerShown: false}} />
            <Stack.Screen name="Login" component={LoginScreen} options={{headerShown: false}} />
            {/* <Stack.Screen name='Home' component={HomeScreen} options={{headerShown: false}} /> */}
        </Stack.Navigator>
    );
}

export default AuthStack
import Animated from 'react-native-reanimated';
import { Text, View, StyleSheet, Image } from 'react-native'
import React, { Component } from 'react'
import Icon from 'react-native-vector-icons/Ionicons';

// navigator 
import { createDrawerNavigator } from '@react-navigation/drawer';

// components
import CustomDrawer from '../components/navigation/CustomDrawer';
import HomeScreen from '../screens/HomeScreen';
import RecettesScreen from '../screens/RecettesScreen';
import ProfileScreen from '../screens/ProfileScreen';
import SettingsScreen from '../screens/SettingsScreen';
import LogoutScreen from '../auth/LogoutScreen';
import RegisterScreen from '../auth/RegisterScreen';



const Drawer = createDrawerNavigator();


function CustomHeader () {
    return (
        <View style={styles.customHeaderContainer}>
            <Text>Pierre-François</Text>
            <Image
                style={styles.profilPic}
                source={require('../img/profile/colibri.png')}            
            />
        </View>
    );
}


const DrawerNavigation = () => {
    return ( 

        <Drawer.Navigator 
            drawerContent={props=><CustomDrawer {...props} />}
            screenOptions={{drawerLabelStyle: {marginLeft: -25}}}            
            >
            <Drawer.Screen 
                name="Home"  
                component={HomeScreen} 
                options={{ 
                    headerTitle: (props) => <CustomHeader {...props} />,
                    drawerIcon: config => <Icon
                    size={23}
                    name={'home'}></Icon>
                    }}
                />
            <Drawer.Screen 
                name="Carnet de recettes" 
                component={RecettesScreen} 
                options={{ 
                    headerTitle: (props) => <CustomHeader {...props} />,
                    drawerIcon: config => <Icon
                    size={23}
                    name={'book'}></Icon>
                    }}/>
            <Drawer.Screen 
                name="Mon compte" 
                component={ProfileScreen} 
                options={{ headerTitle: (props) => <CustomHeader {...props} />,
                drawerIcon: config => <Icon
                    size={23}
                    name={'person'}></Icon> 
                }}/>
            <Drawer.Screen 
                name="Réglages" 
                component={SettingsScreen} 
                options={{ headerTitle: (props) => <CustomHeader {...props} />, 
                drawerIcon: config => <Icon
                    size={23}
                    name={'settings'}></Icon>
            }}/>
            {/* <Drawer.Screen 
                name="Déconnexion" 
                component={LogoutScreen} 
                options={{ headerTitle: (props) => <CustomHeader {...props} />, 
                drawerIcon: config => <Icon
                    size={23}
                    name={'log-out'}></Icon>
            }}/> */}
            <Drawer.Screen 
                name="Register" 
                component={RegisterScreen} 
                options={{ headerTitle: (props) => <CustomHeader {...props} />, 
                drawerIcon: config => <Icon
                    size={23}
                    name={'create'}></Icon>
            , headerShown: false}}/>
            
        </Drawer.Navigator>
    );
}

const AppStack = () => {
    return ( 
        <DrawerNavigation />
    );
}


export default AppStack

const styles = StyleSheet.create({
    customHeaderContainer: {
        flex: 1,
        flexDirection: "row",
        justifyContent:"flex-end", 
        alignItems: "center",
        width: "100%", 
        
    },
    profilPic: {
        width: 30,
        height:30,
        borderRadius: 50,
        marginLeft: 15
    }
})
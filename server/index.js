const express = require('express'); 
const mongoose = require('mongoose');
const cors = require('cors');
require('dotenv/config'); 

const User = require('./models/UserModel');
// const Category = require('./models/category');
// const Recipe = require('./models/recipe');



const app = express();
const router = express.Router();
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use(cors()); 

// routes
const UserRoutes = require('./routes/UserRoutes');
app.use('/users', UserRoutes);
// const CategoryRoutes = require('./routes/categories');
// app.use('/categories', CategoryRoutes);
// const RecipeRoutes = require('./routes/recipes');
// app.use('/recipes', RecipeRoutes);


// mongoDB
const mongoDB = process.env.MONGODB_CRED;
mongoose.connect(mongoDB, { 
    useNewUrlParser: true, 
    useUnifiedTopology: true,
    },
    (req, res) => {
    console.log('Connection to MongoDB up');
});
var db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB connection error"));

app.listen(8080, () => {
    console.log('server listening at port 8080')
}); 
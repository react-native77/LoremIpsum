const mongoose = require('mongoose'); 

const userSchema = mongoose.Schema({
    username: {
        type: String, 
        minlength: 5, 
        required: true
    }, 
    email: {
        type: String, 
        required: true, 
        unique: true
    }, 
    password: {
        type: String, 
        required: true, 
        minlength: 8, 
        maxlength: 2048,        
    }, 
    profilPic: {
        type: String, 
        default: '../img/default/colibri.png'   
    }    
},
{
    timestamps: true,
}
);
module.exports = mongoose.model('User', userSchema);
